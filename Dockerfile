FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/hl7tester-1.0-SNAPSHOT.jar hl7tester-1.0-SNAPSHOT.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/hl7tester-1.0-SNAPSHOT.jar"]