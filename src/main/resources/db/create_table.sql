-- no indexes needed, the whole table is read on startup and cached in memory
create table oidmapping(oid varchar(50), description varchar(200));
alter table oidmapping add constraint oidmapping_pk primary key(oid);
create unique index oidmapping_description_i on oidmapping (description);