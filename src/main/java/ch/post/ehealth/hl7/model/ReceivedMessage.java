package ch.post.ehealth.hl7.model;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ch.post.ehealth.hl7.util.MessageUtil;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class ReceivedMessage {
    private String messageId;
    private String messageAsString;
    private long receiptDate;

    private ReceivedMessage() {
    }

    public ReceivedMessage(String messageId, Message message, long receiptDate) {
        this.messageId = messageId;
        this.receiptDate = receiptDate;
        this.messageAsString = message.toString();
    }

    public ReceivedMessage(String messageId, String messageAsString, long receiptDate) {
        this.messageId = messageId;
        this.receiptDate = receiptDate;
        this.messageAsString = messageAsString;
    }

    @JsonCreator
    public ReceivedMessage(String messageAsString) throws HL7Exception {
        this.messageId = MessageUtil.getInstance().getMessageIdFromMessage(messageAsString);
        this.receiptDate = System.currentTimeMillis();
        this.messageAsString = messageAsString;
    }

    public String getMessageId() {
        return messageId;
    }

    @JsonIgnore
    public Message getMessage() throws HL7Exception {
        return MessageUtil.getInstance().getMessageFromString(messageAsString);
    }

    public long getReceiptDate() {
        return receiptDate;
    }

    public String getMessageAsString() {
        return messageAsString;
    }
}
