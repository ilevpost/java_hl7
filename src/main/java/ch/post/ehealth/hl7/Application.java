package ch.post.ehealth.hl7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages={
        "ch.post.ehealth.hl7.messagestore",
        "ch.post.ehealth.hl7.controller",
        "ch.post.ehealth.hl7.sendandreceive",
        "ch.post.ehealth.hl7.util"
})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
