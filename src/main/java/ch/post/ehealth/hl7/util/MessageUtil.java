package ch.post.ehealth.hl7.util;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MessageUtil {
    private static final MessageUtil instance = new MessageUtil();

    private MessageUtil() {
    }

    public static MessageUtil getInstance() {
        return instance;
    }

    public Message getMessageFromString(String message) throws HL7Exception {
        HapiContext context = new DefaultHapiContext();
        PipeParser parser = context.getPipeParser();
        return parser.parse(message);
    }

    public String getMessageIdFromMessage(Message message) throws HL7Exception {
        Terser terser = new Terser(message);
        return terser.get("/.MSH-10-1");
    }

    public String getMessageIdFromMessage(String message) throws HL7Exception {
        return getMessageIdFromMessage(getMessageFromString(message));
    }

    public String stripMessage(String message) {
        String strippedMessage = message.replace("\r", "\n");
        StringTokenizer tokenizer = new StringTokenizer(strippedMessage, "\n");
        List<StringBuilder> strippedTokens = new ArrayList<>();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            StringBuilder tokenBuilder = new StringBuilder(token);
            while (tokenBuilder.charAt(tokenBuilder.length() - 1) == '|' || tokenBuilder.charAt(tokenBuilder.length() - 1) == '^') {
                tokenBuilder.deleteCharAt(tokenBuilder.length() - 1);
            }
            strippedTokens.add(tokenBuilder);
        }
        StringBuilder strippedMessageBuilder = new StringBuilder();
        for (StringBuilder token : strippedTokens) {
            strippedMessageBuilder.append(token);
        }

        return strippedMessageBuilder.toString();
    }

}
