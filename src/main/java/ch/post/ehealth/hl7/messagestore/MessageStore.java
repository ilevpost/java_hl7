package ch.post.ehealth.hl7.messagestore;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ch.post.ehealth.hl7.model.ReceivedMessage;

import java.util.Map;

public interface MessageStore {
    ReceivedMessage addReceivedMessage(ReceivedMessage message);
    ReceivedMessage addReceivedMessage(Message message) throws HL7Exception;
    ReceivedMessage addReceivedMessage(String message) throws HL7Exception;
    ReceivedMessage getReceivedMessage(String messageId);
    ReceivedMessage removeReceivedMessage(String messageId);
    Map<String, ReceivedMessage> getMessagesReceived();
    public void clear();
}
