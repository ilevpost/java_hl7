package ch.post.ehealth.hl7.messagestore;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ch.post.ehealth.hl7.model.ReceivedMessage;
import ch.post.ehealth.hl7.util.MessageUtil;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class MessageStoreImpl implements MessageStore {
    private final Map<String, ReceivedMessage> messagesReceived = new ConcurrentHashMap<>();

    public MessageStoreImpl() {
    }

    @Override
    public ReceivedMessage addReceivedMessage(ReceivedMessage message) {
        messagesReceived.put(message.getMessageId(), message);
        return messagesReceived.get(message.getMessageId());
    }

    @Override
    public ReceivedMessage addReceivedMessage(Message message) throws HL7Exception {
        String messageId = MessageUtil.getInstance().getMessageIdFromMessage(message);
        ReceivedMessage receivedMessage = new ReceivedMessage(messageId, message, System.currentTimeMillis());
        return addReceivedMessage(receivedMessage);
    }

    @Override
    public ReceivedMessage addReceivedMessage(String message) throws HL7Exception {
        return addReceivedMessage(new ReceivedMessage(message));
    }

    @Override
    public ReceivedMessage getReceivedMessage(String messageId) {
        return messagesReceived.get(messageId);
    }

    @Override
    public ReceivedMessage removeReceivedMessage(String messageId) {
        return messagesReceived.remove(messageId);
    }

    @Override
    public Map<String, ReceivedMessage> getMessagesReceived() {
        return Collections.unmodifiableMap(messagesReceived);
    }

    @Override
    public void clear() {
        messagesReceived.clear();
    }
}
