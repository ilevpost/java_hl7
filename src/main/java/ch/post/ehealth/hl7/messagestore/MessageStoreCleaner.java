package ch.post.ehealth.hl7.messagestore;

import ch.post.ehealth.hl7.model.ReceivedMessage;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class MessageStoreCleaner {
    private static final long CLEANING_INTERVAL = 60L * 60L * 1000;
    @Autowired
    MessageStore messageStore;

    public void cleanMessageStore() {
        Map<String, ReceivedMessage> messages = messageStore.getMessagesReceived();
        for (Map.Entry<String, ReceivedMessage> entry : messages.entrySet()) {
            if (System.currentTimeMillis() - entry.getValue().getReceiptDate() > CLEANING_INTERVAL) {
                messageStore.removeReceivedMessage(entry.getKey());
            }
        }
    }
}
