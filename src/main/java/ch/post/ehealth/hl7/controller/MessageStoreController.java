package ch.post.ehealth.hl7.controller;

import ca.uhn.hl7v2.HL7Exception;
import ch.post.ehealth.hl7.messagestore.MessageStore;
import ch.post.ehealth.hl7.model.ReceivedMessage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("messages")
public class MessageStoreController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageStoreController.class);

    @Autowired
    MessageStore messageStore;

    @GetMapping(value = "/message/{messageId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReceivedMessage> getReceivedMessage(@PathVariable String messageId) {
        LOGGER.info("Received a get request for the message with id " + messageId);
        ReceivedMessage receivedMessage = messageStore.getReceivedMessage(messageId);
        if (receivedMessage == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(receivedMessage);
        }
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<ReceivedMessage>> getAllReceivedMessages() {
        LOGGER.info("Received a get request for all the messages...");
        return ResponseEntity.ok(messageStore.getMessagesReceived().values());
    }

    @DeleteMapping(value = "/message/{messageId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReceivedMessage> deleteReceivedMessage(@PathVariable String messageId) {
        LOGGER.info("Received a delete request for the message with id " + messageId);
        ReceivedMessage receivedMessage = messageStore.getReceivedMessage(messageId);
        if (receivedMessage != null) {
            return ResponseEntity.ok(messageStore.removeReceivedMessage(messageId));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(value = "/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReceivedMessage> addReceivedMessage(@RequestBody ReceivedMessage inputMessage) throws HL7Exception {
        String infoPart = StringUtils.abbreviate(inputMessage.getMessageAsString(), 100);
        LOGGER.info("Received an add request for the following message starting with " + infoPart);
        ReceivedMessage receivedMessage = messageStore.addReceivedMessage(inputMessage.getMessageAsString());
        if (receivedMessage != null) {
            return ResponseEntity.ok(receivedMessage);
        } else {
            return ResponseEntity.unprocessableEntity().body(receivedMessage);
        }

    }

    public MessageStore getMessageStore() {
        return messageStore;
    }
}
