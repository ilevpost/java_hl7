package ch.post.ehealth.hl7.sendandreceive;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.HL7Service;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class MessageListener implements ReceivingApplication<Message> {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageListener.class);
    private HL7Service hl7Server;
    private int listeningPort;
    private boolean useTlsForListening;
    private boolean isListeningServerStarted;
    private MessageHandler messageHandler;

    private MessageListener() {

    }

    public MessageListener(final int listeningPort, final boolean useTlsForListening, MessageHandler messageHandler) {
        this.listeningPort = listeningPort;
        this.useTlsForListening = useTlsForListening;
        this.messageHandler = messageHandler;
    }

    /**
     * starts up and configures an hl7 listener
     * @return the LLPMessageTester object for chaining
     */
    public synchronized MessageListener startupListener() {
        HapiContext context = new DefaultHapiContext();
        hl7Server = context.newServer(listeningPort, useTlsForListening);
        hl7Server.registerApplication("ADT", "A01", this);
        try {
            hl7Server.startAndWait();
            LOGGER.info("HL7 Listener successfully started on port " + listeningPort);
        } catch (InterruptedException e) {
            LOGGER.error("HL7 Listener Interrupted ", e);
        }

        isListeningServerStarted = true;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public boolean canProcess(Message theIn) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public Message processMessage(Message theMessage, Map<String, Object> theMetadata) throws ReceivingApplicationException, HL7Exception {
        LOGGER.info("Listener received message:\n" + theMessage.toString().replace("\r", "\n"));
        messageHandler.storeMessage(theMessage.toString());
        // Now generate a simple acknowledgment message and return it
        try {
            return theMessage.generateACK();
        } catch (IOException e) {
            throw new HL7Exception(e);
        }
    }

    /**
     * Shuts down the hl7 messages listener
     */
    public void shutdownListener() {
        hl7Server.stopAndWait();
        isListeningServerStarted = false;
    }

    public boolean isListeningServerStarted() {
        return isListeningServerStarted;
    }
}
