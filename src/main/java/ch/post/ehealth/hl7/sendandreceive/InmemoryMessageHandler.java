package ch.post.ehealth.hl7.sendandreceive;

import ca.uhn.hl7v2.HL7Exception;
import ch.post.ehealth.hl7.messagestore.MessageStore;
import ch.post.ehealth.hl7.model.ReceivedMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Collections;


public class InmemoryMessageHandler implements MessageHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(InmemoryMessageHandler.class);
    @Autowired
    MessageStore messageStore;

    @Override
    public void storeMessage(String message) {
        try {
            messageStore.addReceivedMessage(message);
        } catch (HL7Exception e) {
            LOGGER.error("Could not store the message", e);
        }
    }

    @Override
    public ReceivedMessage retrieveMessage(String messageId) {
        return messageStore.getReceivedMessage(messageId);
    }

    @Override
    public Collection<ReceivedMessage> retrieveAllMessages() {
        return Collections.unmodifiableCollection(messageStore.getMessagesReceived().values());
    }
}
