package ch.post.ehealth.hl7.sendandreceive;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.HL7Service;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class LLPMessageTester implements ReceivingApplication<Message> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LLPMessageTester.class);
    private String outputMessage;
    private HL7Service hl7Server;
    private int listeningPort;
    private boolean useTlsForListening;
    private boolean isListeningServerStarted;
    private boolean messageReceived;

    private LLPMessageTester() {

    }

    public LLPMessageTester(final int listeningPort, final boolean useTlsForListening) {
        this.listeningPort = listeningPort;
        this.useTlsForListening = useTlsForListening;
    }

    /**
     * starts up and configures an hl7 listener
     * @return the LLPMessageTester object for chaining
     */
    public synchronized LLPMessageTester startupListener() {
        HapiContext context = new DefaultHapiContext();
        hl7Server = context.newServer(listeningPort, useTlsForListening);
        hl7Server.registerApplication("ADT", "A01", this);
        // TODO register other expected message types too
        try {
            hl7Server.startAndWait();
            LOGGER.info("HL7 Listener successfully started on port " + listeningPort);
        } catch (InterruptedException e) {
            LOGGER.error("HL7 Listener Interrupted ", e);
        }

        isListeningServerStarted = true;
        return this;
    }

    public LLPMessageTester sendMessage(String message, String host, int port) throws HL7Exception, IOException, LLPException {
        return sendMessage(message, host, port, false);
    }

    /**
     * Sends a message to the specified host and port
     *
     * @param inputMessage The message to send to the middleware
     * @param host         The host where the middleware resides
     * @param port         The listening port of the middleware
     * @param useTls       Whether to use SSL/TLS for the communication with the middleware
     * @return the LLPMessageTester object for chaining
     * @throws HL7Exception
     * @throws IOException
     * @throws LLPException
     */
    public LLPMessageTester sendMessage(String inputMessage, String host, int port, boolean useTls) throws HL7Exception, IOException, LLPException {
        MessageSender.getInstance().sendMessage(inputMessage, host, port, useTls);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public boolean canProcess(Message theIn) {
        return true;
    }


    /**
     * {@inheritDoc}
     */
    public Message processMessage(Message theMessage, Map<String, Object> theMetadata) throws ReceivingApplicationException, HL7Exception {
        LOGGER.info("Listener received message:\n" + theMessage.toString().replace("\r", "\n"));
        outputMessage = new DefaultHapiContext().getPipeParser().encode(theMessage);
        messageReceived = true;
        // Now generate a simple acknowledgment message and return it
        try {
            return theMessage.generateACK();
        } catch (IOException e) {
            throw new HL7Exception(e);
        }
    }

    private String stripMessage(String message) {
        String strippedMessage = message.replace("\r", "\n");
        StringTokenizer tokenizer = new StringTokenizer(strippedMessage, "\n");
        List<StringBuilder> strippedTokens = new ArrayList<>();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            StringBuilder tokenBuilder = new StringBuilder(token);
            while (tokenBuilder.charAt(tokenBuilder.length() - 1) == '|') {
                tokenBuilder.deleteCharAt(tokenBuilder.length() - 1);
            }
            strippedTokens.add(tokenBuilder);
        }
        StringBuilder strippedMessageBuilder = new StringBuilder();
        for (StringBuilder token : strippedTokens) {
            strippedMessageBuilder.append(token);
        }

        return strippedMessageBuilder.toString();
    }

    /**
     * Sends a message to the middleware, expects to receive an answer with the transformed message (via method processMessage)
     * and checks whether the received message is as expected
     *
     * @param inputMessage               The message to send to the middleware
     * @param expectedTransformedMessage The expected transformed message
     * @param sendHost                   The host where the middleware resides
     * @param sendPort                   The listening port of the middleware
     * @param useTlsForSending           Whether to use SSL/TLS for the communication with the middleware
     * @return true if the process runs ok and the expected and received messages match
     */
    public synchronized boolean sendAndReceiveMessage(String inputMessage, String expectedTransformedMessage,
                                                      String sendHost, int sendPort, boolean useTlsForSending) {
        try {
            if (!isListeningServerStarted) {
                startupListener();
            }
            messageReceived = false;
            sendMessage(inputMessage, sendHost, sendPort, useTlsForSending);
            long startTime = System.currentTimeMillis();
            // wait 10 seconds for the listener to process the message
            long timeoutInMillis = 1000 * 10;
            while (!messageReceived) {
                Long currentTime = System.currentTimeMillis();
                if (currentTime - startTime > timeoutInMillis) {
                    break;
                }
            }
            String strippedOutputMessage = stripMessage(outputMessage);
            String strippedExpectedMessage = stripMessage(expectedTransformedMessage);
            boolean expectedAndReceivedMessagesMatch = strippedOutputMessage.equals(strippedExpectedMessage);
            if (expectedAndReceivedMessagesMatch) {
                LOGGER.info("Test case successful, expected and received messages match");
            } else {
                LOGGER.warn("Test case unsuccessful, expected and received messages DO NOT match");
                LOGGER.warn("Received message");
                LOGGER.info(strippedOutputMessage);
                LOGGER.warn("Expected message");
                LOGGER.info(strippedExpectedMessage);
            }
            return expectedAndReceivedMessagesMatch;
        } catch (Exception e) {
            LOGGER.error("An error occurred while trying to send the message", e);
            return false;
        }
    }

    /**
     * Sends multiple messages to the middleware, expects to receive answera with the transformed messages (via method processMessage)
     * and checks whether the received messages are as expected
     *
     * @param inputMessagesAndExpectedOutputs A map containing the messages to send to the middleware and the expected transformed messages as pairs
     * @param sendHost                        The host where the middleware resides
     * @param sendPort                        The listening port of the middleware
     * @param useTlsForSending                Whether to use SSL/TLS for the communication with the middleware
     * @return true if the process runs ok and the expected and received messages match
     */
    public synchronized Map<String, Boolean> sendAndReceiveMultipleMessages(Map<String, String> inputMessagesAndExpectedOutputs,
                                                                            String sendHost, int sendPort, boolean useTlsForSending) {
        Map<String, Boolean> sendResults = new HashMap<>();
        for (Map.Entry<String, String> entry : inputMessagesAndExpectedOutputs.entrySet()) {
            String inputMessage = entry.getKey();
            String expectedOutput = entry.getValue();
            boolean result = sendAndReceiveMessage(inputMessage, expectedOutput, sendHost, sendPort, useTlsForSending);
            sendResults.put(inputMessage, result);
        }
        return sendResults;
    }

    /**
     * Shuts down the hl7 messages listener
     */
    public void shutdownListener() {
        hl7Server.stopAndWait();
        isListeningServerStarted = false;
    }
}
