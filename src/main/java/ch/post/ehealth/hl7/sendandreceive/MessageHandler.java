package ch.post.ehealth.hl7.sendandreceive;

import ch.post.ehealth.hl7.model.ReceivedMessage;

import java.util.Collection;

public interface MessageHandler {
    void storeMessage(String message);
    ReceivedMessage retrieveMessage(String messageId);
    Collection<ReceivedMessage> retrieveAllMessages();
}
