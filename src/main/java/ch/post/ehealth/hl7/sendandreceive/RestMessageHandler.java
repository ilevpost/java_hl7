package ch.post.ehealth.hl7.sendandreceive;

import ca.uhn.hl7v2.HL7Exception;
import ch.post.ehealth.hl7.model.ReceivedMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;


public class RestMessageHandler implements MessageHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestMessageHandler.class);
    private final String restServiceUrl;

    public RestMessageHandler(String restServiceUrl) {
        this.restServiceUrl = restServiceUrl;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Override
    public void storeMessage(String message) {
        try {
            restTemplate().postForObject(restServiceUrl + "/messages/message", new ReceivedMessage(message), ReceivedMessage.class);
        } catch (HL7Exception e) {
            LOGGER.error("Could not store the message", e);
        }
    }

    @Override
    public ReceivedMessage retrieveMessage(String messageId) {
        return restTemplate().getForObject(restServiceUrl + "/messages/message/" + messageId, ReceivedMessage.class);
    }

    @Override
    public Collection<ReceivedMessage> retrieveAllMessages() {
        return  restTemplate().getForObject(restServiceUrl + "/messages/all", Collection.class);
    }
}
