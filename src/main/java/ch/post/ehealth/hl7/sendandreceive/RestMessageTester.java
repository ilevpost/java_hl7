package ch.post.ehealth.hl7.sendandreceive;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.llp.LLPException;
import ch.post.ehealth.hl7.model.ReceivedMessage;
import ch.post.ehealth.hl7.util.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class RestMessageTester {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestMessageTester.class);
    MessageHandler messageHandler;

    public RestMessageTester() {
    }

    public RestMessageTester(final String messagesServerUrl) {
        messageHandler = new RestMessageHandler(messagesServerUrl);
    }

    public RestMessageTester(final MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    /**
     * Sends a message to the specified host and port
     *
     * @param inputMessage The message to send to the middleware
     * @param host         The host where the middleware resides
     * @param port         The listening port of the middleware
     * @param useTls       Whether to use SSL/TLS for the communication with the middleware
     * @return the LLPMessageTester object for chaining
     * @throws HL7Exception
     * @throws IOException
     * @throws LLPException
     */
    private RestMessageTester sendMessage(String inputMessage, String host, int port, boolean useTls) throws HL7Exception, IOException, LLPException {
        MessageSender.getInstance().sendMessage(inputMessage, host, port, useTls);
        return this;
    }

    private int firstMismatch(String org, String check) {
        int limit = (org.length() > check.length()) ? org.length() : check.length();
        for (int i = 0; i < limit; i++) {
            if (org.charAt(i) != check.charAt(i)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Sends a message to the middleware, expects to receive an answer with the transformed message (via method processMessage)
     * and checks whether the received message is as expected
     *
     * @param inputMessage               The message to send to the middleware
     * @param expectedTransformedMessage The expected transformed message
     * @param sendHost                   The host where the middleware resides
     * @param sendPort                   The listening port of the middleware
     * @param useTlsForSending           Whether to use SSL/TLS for the communication with the middleware
     * @return true if the process runs ok and the expected and received messages match
     */
    public synchronized boolean sendAndReceiveMessage(String inputMessage, String expectedTransformedMessage,
                                                      String sendHost, int sendPort, boolean useTlsForSending) {
        try {
            String messageId = MessageUtil.getInstance().getMessageIdFromMessage(inputMessage);
            sendMessage(inputMessage, sendHost, sendPort, useTlsForSending);

            long startTime = System.currentTimeMillis();
            // wait 2 seconds for the listener to process the message
            long timeoutInMillis = 1000 * 2;
            while (true) {
                Long currentTime = System.currentTimeMillis();
                if (currentTime - startTime > timeoutInMillis) {
                    break;
                }
            }
            ReceivedMessage receivedMessage = messageHandler.retrieveMessage(messageId);

            String strippedOutputMessage = MessageUtil.getInstance().stripMessage(receivedMessage.getMessageAsString());
            String strippedExpectedMessage = MessageUtil.getInstance().stripMessage(expectedTransformedMessage);
            boolean expectedAndReceivedMessagesMatch = strippedOutputMessage.equals(strippedExpectedMessage);
            if (expectedAndReceivedMessagesMatch) {
                LOGGER.info("Test case successful, expected and received messages match");
            } else {
                int firstMismatchingCharacter = firstMismatch(strippedOutputMessage, strippedExpectedMessage);
                String mismatchingPart = strippedOutputMessage.substring(firstMismatchingCharacter);
                LOGGER.warn("Test case unsuccessful, expected and received messages DO NOT match");
                LOGGER.warn("Received message");
                LOGGER.info(strippedOutputMessage);
                LOGGER.warn("Expected message");
                LOGGER.info(strippedExpectedMessage);
                LOGGER.warn("Mismatching position=" + firstMismatchingCharacter + " Mismatching part follows:");
                LOGGER.warn(mismatchingPart);
            }
            return expectedAndReceivedMessagesMatch;
        } catch (Exception e) {
            LOGGER.error("An error occurred while trying to send the message", e);
            return false;
        }
    }

    /**
     * Sends multiple messages to the middleware, expects to receive answera with the transformed messages (via method processMessage)
     * and checks whether the received messages are as expected
     *
     * @param inputMessagesAndExpectedOutputs A map containing the messages to send to the middleware and the expected transformed messages as pairs
     * @param sendHost                        The host where the middleware resides
     * @param sendPort                        The listening port of the middleware
     * @param useTlsForSending                Whether to use SSL/TLS for the communication with the middleware
     * @return true if the process runs ok and the expected and received messages match
     */
    public synchronized Map<String, Boolean> sendAndReceiveMultipleMessages(Map<String, String> inputMessagesAndExpectedOutputs,
                                                                            String sendHost, int sendPort, boolean useTlsForSending) {
        Map<String, Boolean> sendResults = new HashMap<>();
        for (Map.Entry<String, String> entry : inputMessagesAndExpectedOutputs.entrySet()) {
            String inputMessage = entry.getKey();
            String expectedOutput = entry.getValue();
            boolean result = sendAndReceiveMessage(inputMessage, expectedOutput, sendHost, sendPort, useTlsForSending);
            sendResults.put(inputMessage, result);
        }
        return sendResults;
    }
}
