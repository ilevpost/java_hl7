package ch.post.ehealth.hl7.sendandreceive;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

public class ADTRestTester {
    protected static final String ADT_MIRTH_RECEIVING_PORT_DEFAULT = "6661";
    protected static final Integer ADT_MIRTH_RECEIVING_PORT = Integer.valueOf(Optional.ofNullable(System.getProperty("MIRTH_PORT")).orElse(ADT_MIRTH_RECEIVING_PORT_DEFAULT));
    protected static final String ADT_MIRTH_RECEIVING_HOST_DEFAULT = "localhost";
    protected static final String ADT_MIRTH_RECEIVING_HOST = Optional.ofNullable(System.getProperty("MIRTH_HOST")).orElse(ADT_MIRTH_RECEIVING_HOST_DEFAULT);

    protected static final String SERVER_URL_DEFAULT = "http://localhost:9800";
    protected static final String SERVER_URL = Optional.ofNullable(System.getProperty("SERVER_URL")).orElse(SERVER_URL_DEFAULT);

    protected RestMessageTester adtMessageTester;
    private static final Logger LOGGER = LoggerFactory.getLogger(ADTRestTester.class);

    @Before
    public void initialize() {
        adtMessageTester = new RestMessageTester(SERVER_URL);
    }

    @Bean
    public MessageHandler getMessageHandler() {
        return new RestMessageHandler(SERVER_URL);
    }

    @Test
    public void testMessageTransformation() throws Exception {
        String msg = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489aa||2.2|||||||||\n" +
                "EVN||20180522102452|||MusterFritz||\n" +
                "PID|||1412199415782^^^SIVC-PAS-PROD^PI||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^^PH~+41815300690^^PH~^NET^Internet^patrick.braunschweiler@ksgr.ch|+41815300689^^PH~+41815300690^^PH~^NET^Internet^patrick.braunschweiler@ksgr.ch|||||||||||||||\n" +
                "ROL||AD||123456^Lang^Franz^^^Dr. med.^^^Healthcase Provider Directory^||||||||||\n" +
                "PV1||I|||||||||||||||||1234567890|||||||||||||||||||||||||201805241030|\n";
        String expectedMsg =
                "MSH|^~\\&|SIVC-PAS-PROD^2.16.756.5.30.1.166.3.1.1^ISO|SIVC^2.16.756.5.30.1.166.3^ISO|PIX/PDQ Manager^2.16.756.5.30.1.166.0.12.1.3.1.6^ISO|^2.16.756.5.30.1.166.0^ISO|20180522102452||ADT^A01^ADT_A01|12EF35AB489aa|T|2.5|||||||||\n" +
                "EVN||20180522102452|||MusterFritz^^^^^^^^SIVC-PAS-PROD&2.16.756.5.30.1.166.3.1.1&ISO||\n" +
                "PID|||1412199415782^^^SIVC-PAS-PROD&2.16.756.5.30.1.166.3.1.1&ISO^PI||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^PRN^PH~+41815300690^PRN^PH~^NET^Internet^patrick.braunschweiler@ksgr.ch|+41815300689^WPN^PH~+41815300690^WPN^PH~^NET^Internet^patrick.braunschweiler@ksgr.ch|||||||||||||||\n" +
                "ROL||AD|PP|123456^Lang^Franz^^^Dr. med.^^^Healthcase Provider Directory&2.16.756.5.30.1.166.0.12.1.3.1.15&ISO^||||||||||\n" +
                "PV1||I|||||||||||||||||1234567890^^^SIVC-PAS-PROD&2.16.756.5.30.1.166.3.1.1&ISO|||||||||||||||||||||||||201805241030|999912311000\n";

        // send a version of HL7 that shouldn't be changed by the middleware
        String msg2 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489bb||2.6|||||||||\n" +
                "EVN||20180522102452|||MusterFritz||\n" +
                "PID|||1412199415782^^^SIVC-PAS-PROD^PI||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^^PH~+41815300690^^PH~^NET^Internet^patrick.braunschweiler@ksgr.ch|+41815300689^^PH~+41815300690^^PH~^NET^Internet^patrick.braunschweiler@ksgr.ch|||||||||||||||\n" +
                "ROL||AD||123456^Lang^Franz^^^Dr. med.^^^Healthcase Provider Directory^||||||||||\n" +
                "PV1||I|||||||||||||||||1234567890|||||||||||||||||||||||||201805241030|\n";
        String expectedMsg2 =
                "MSH|^~\\&|SIVC-PAS-PROD^2.16.756.5.30.1.166.3.1.1^ISO|SIVC^2.16.756.5.30.1.166.3^ISO|PIX/PDQ Manager^2.16.756.5.30.1.166.0.12.1.3.1.6^ISO|^2.16.756.5.30.1.166.0^ISO|20180522102452||ADT^A01^ADT_A01|12EF35AB489bb|T|2.6|||||||||\n" +
                        "EVN||20180522102452|||MusterFritz^^^^^^^^SIVC-PAS-PROD&2.16.756.5.30.1.166.3.1.1&ISO||\n" +
                        "PID|||1412199415782^^^SIVC-PAS-PROD&2.16.756.5.30.1.166.3.1.1&ISO^PI||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^PRN^PH~+41815300690^PRN^PH~^NET^Internet^patrick.braunschweiler@ksgr.ch|+41815300689^WPN^PH~+41815300690^WPN^PH~^NET^Internet^patrick.braunschweiler@ksgr.ch|||||||||||||||\n" +
                        "ROL||AD|PP|123456^Lang^Franz^^^Dr. med.^^^Healthcase Provider Directory&2.16.756.5.30.1.166.0.12.1.3.1.15&ISO^||||||||||\n" +
                        "PV1||I|||||||||||||||||1234567890^^^SIVC-PAS-PROD&2.16.756.5.30.1.166.3.1.1&ISO|||||||||||||||||||||||||201805241030|999912311000\n";

        Map<String, String> messagesWithExpectedValues = new HashMap<>();
        messagesWithExpectedValues.put(msg, expectedMsg);
        messagesWithExpectedValues.put(msg2, expectedMsg2);

        Map<String, Boolean> testResults = adtMessageTester.sendAndReceiveMultipleMessages(messagesWithExpectedValues, ADT_MIRTH_RECEIVING_HOST, ADT_MIRTH_RECEIVING_PORT, false);
        LOGGER.info("\n\n------------- TEST RESULTS ----------------------------\n");
        for (Map.Entry<String, Boolean> result : testResults.entrySet()) {
            boolean testOutcome = result.getValue();
            String resultOutput = testOutcome ? "transformed successfully" : " !!! TRANSFORMATION PROCESS FAILED !!!";
            LOGGER.info("Following message test " + resultOutput + "\n" + result.getKey());
            LOGGER.info(".............. \n");
        }
        for (Map.Entry<String, Boolean> result : testResults.entrySet()) {
            assertTrue(result.getValue());
        }
    }
}
