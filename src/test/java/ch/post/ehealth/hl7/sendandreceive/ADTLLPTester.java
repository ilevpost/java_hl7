package ch.post.ehealth.hl7.sendandreceive;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

public class ADTLLPTester {
    protected static final String ADT_MIRTH_RECEIVING_PORT_DEFAULT = "6661";
    protected static final Integer ADT_MIRTH_RECEIVING_PORT = Integer.valueOf(Optional.ofNullable(System.getProperty("MIRTH_PORT")).orElse(ADT_MIRTH_RECEIVING_PORT_DEFAULT));
    protected static final String ADT_MIRTH_RECEIVING_HOST_DEFAULT = "localhost";
    protected static final String ADT_MIRTH_RECEIVING_HOST = Optional.ofNullable(System.getProperty("MIRTH_HOST")).orElse(ADT_MIRTH_RECEIVING_HOST_DEFAULT);
    protected static final String ADT_LISTENER_PORT_DEFAULT = "5663";
    protected static final Integer ADT_LISTENER_PORT = Integer.valueOf(Optional.ofNullable(System.getProperty("LISTENER_PORT")).orElse(ADT_LISTENER_PORT_DEFAULT));

    protected LLPMessageTester adtLLPMessageTester;
    private static final Logger LOGGER = LoggerFactory.getLogger(ADTLLPTester.class);

    @Before
    public void initialize() {
        adtLLPMessageTester = new LLPMessageTester(ADT_LISTENER_PORT, false);
        adtLLPMessageTester.startupListener();
    }

    @After
    public void shutDownListener() {
        adtLLPMessageTester.shutdownListener();
    }

    @Test
    @Ignore
    public void testMessageTransformation() throws Exception {
        String msg = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489aa||2.2|||||||||\n" +
                "EVN||20180522102452|||KSGR^Muster^Fritz^012344||\n" +
                "PID|||1412199415782^^^SIVC-PAS-PROD^PI^SIVC~7569999888877^^^NeueAHVNr^SS^NeueAHVNr~80756000080069520760^^^VK_NR^GV^VK_NR~80756000069520760^^^VK_NR^ZV^VK_NR||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\n" +
                "ROL||AD|PP|123456^^^^^^^^Healthcase Provider Directory||||||||||\n" +
                "PV1||I^stationaer|Sta3^200.1^^KSGR|||||4712^Lang^Franz^^^Dr.^^^^^^^^^^^^^|4712^Lang^Franz^^^Dr.^^^^^^^^^^^^^||||||||||1234567890^^^KSGR|102|KVG|||||||||||||||||||||||201805241030|\n";
        String expectedMsg = "MSH|^~\\&|SIVC-PAS-PROD^^2.16.756.5.30.1.166.3.1.1|SIVC^2.16.756.5.30.1.166.3|PIX/PDQ Manager^2.16.756.5.30.1.166.0.12.1.3.1.6|^2.16.756.5.30.1.166.0|20180522102452||ADT^A01|12EF35AB489aa|P|2.5|||||||||\n" +
                "EVN||20180522102452|||KSGR^Muster^Fritz^012344||\n" +
                "PID|||1412199415782^^^SIVC-PAS-PROD&2.16.756.5.30.1.166.3.1.1&ISO^PI^SIVC&2.16.756.5.30.1.166.3&ISO~7569999888877^^^NeueAHVNr&2.16.756.5.32&ISO^SS^NeueAHVNr&2.16.756.5.32&ISO~80756000080069520760^^^VK_NR&2.16.756.5.30.1.123&ISO^GV^VK_NR&2.16.756.5.30.1.123&ISO~80756000069520760^^^VK_NR&2.16.756.5.30.1.123&ISO^ZV^VK_NR&2.16.756.5.30.1.123&ISO||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||^^PH^^41^815300689~^^CP^^41^792338481~^NET^internet^p.braunschweiler@gmail.com~^^FX^^41^792338481|^^BP^^41^815300689~^^CP^^41^792338481~^NET^internet^p.braunschweiler@gmail.com||||||||||||||||\n" +
                "ROL||AD|PP|123456^^^^^^^^Healthcase Provider Directory&2.16.756.5.30.1.166.0.12.1.3.1.15&ISO||||||||||\n" +
                "PV1||I^stationaer|Sta3^200.1^^KSGR&2.16.756.5.30.1..bcd.1|||||4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6&ISO^L|4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6&ISO^L||||||||||1234567890^^^KSGR&2.16.756.5.30.1..bcd.1&ISO|Allgemein|KVG|||||||||||||||||||||||201805241030|999912311000\n";

        String msg2 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489bb||2.2|||||||||\n" +
                "EVN||20190522102452|||KSGR^Muster^Fritz^012344||\n" +
                "PID|||1412199415782^^^SIVC-PAS-PROD^PI^SIVC~7569999888877^^^NeueAHVNr^SS^NeueAHVNr~80756000080069520760^^^VK_NR^GV^VK_NR~80756000069520760^^^VK_NR^ZV^VK_NR||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\n" +
                "PV1||I^stationaer|Sta3^200.1^^KSGR|||||4712^Lang^Franz^^^Dr.^^^^^^^^^^^^^|4712^Lang^Franz^^^Dr.^^^^^^^^^^^^^||||||||||1234567890^^^KSGR|102|KVG|||||||||||||||||||||||201805241030|\n";

                String expectedMsg2 = "MSH|^~\\&|SIVC-PAS-PROD^^2.16.756.5.30.1.166.3.1.1|SIVC^2.16.756.5.30.1.166.3|PIX/PDQ Manager^2.16.756.5.30.1.166.0.12.1.3.1.6|^2.16.756.5.30.1.166.0|20180522102452||ADT^A01|12EF35AB489bb|P|2.5|||||||||\n" +
                "EVN||20190522102452|||KSGR^Muster^Fritz^012344||\n" +
                "PID|||1412199415782^^^SIVC-PAS-PROD&2.16.756.5.30.1.166.3.1.1&ISO^PI^SIVC&2.16.756.5.30.1.166.3&ISO~7569999888877^^^NeueAHVNr&2.16.756.5.32&ISO^SS^NeueAHVNr&2.16.756.5.32&ISO~80756000080069520760^^^VK_NR&2.16.756.5.30.1.123&ISO^GV^VK_NR&2.16.756.5.30.1.123&ISO~80756000069520760^^^VK_NR&2.16.756.5.30.1.123&ISO^ZV^VK_NR&2.16.756.5.30.1.123&ISO||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||^^PH^^41^815300689~^^CP^^41^792338481~^NET^internet^p.braunschweiler@gmail.com~^^FX^^41^792338481|^^BP^^41^815300689~^^CP^^41^792338481~^NET^internet^p.braunschweiler@gmail.com||||||||||||||||\n" +
                "PV1||I^stationaer|Sta3^200.1^^KSGR&2.16.756.5.30.1..bcd.1|||||4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6&ISO^L|4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6&ISO^L||||||||||1234567890^^^KSGR&2.16.756.5.30.1..bcd.1&ISO|Allgemein|KVG|||||||||||||||||||||||201805241030|999912311000\n";

        Map<String, String> messagesWithExpectedValues = new HashMap<>();
        messagesWithExpectedValues.put(msg, expectedMsg);
        messagesWithExpectedValues.put(msg2, expectedMsg2);

        Map<String, Boolean> testResults = adtLLPMessageTester.sendAndReceiveMultipleMessages(messagesWithExpectedValues, ADT_MIRTH_RECEIVING_HOST, ADT_MIRTH_RECEIVING_PORT, false);
        LOGGER.info("\n\n------------- TEST RESULTS ----------------------------\n");
        for (Map.Entry<String, Boolean> result : testResults.entrySet()) {
            boolean testOutcome = result.getValue();
            String resultOutput = testOutcome ? "transformed successfully" : " !!! TRANSFORMATION PROCESS FAILED !!!";
            LOGGER.info("Following message test " + resultOutput + "\n" + result.getKey());
            LOGGER.info(".............. \n");
        }
        for (Map.Entry<String, Boolean> result : testResults.entrySet()) {
            assertTrue(result.getValue());
        }
    }
}
