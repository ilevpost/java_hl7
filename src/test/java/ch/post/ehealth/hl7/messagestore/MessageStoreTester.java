package ch.post.ehealth.hl7.messagestore;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MessageStoreTester {
    final String msg1 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489aa||2.2|||||||||\n" +
            "EVN||20180522102452|||KSGR^Muster^Fritz^012344||\n" +
            "PID|||141" +
            "2199415782^80756000080069520760~CSS^80756000080069520760~CSS^KSGR&KSGR-DISPO-PROD&2.16.756.5.30.1.456.1&ISO^PI^KSGR~7569999888877^^^AHV Nummer neu&2.16.756.5.32&ISO^SS||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\n" +
            "PV1||I^^|Sta3^200.1^^KSGR||||4711^Langer^Bernhard^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4713^Langhardt^Peter^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6||||||||||1234567890|Allgemein|KVG|||||||||||||||||||||||201805241030|\n";
    final String msg2 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489aabb||2.2|||||||||\n" +
            "EVN||20180522102452|||KSGR^Muster^Fritz^012344567||\n" +
            "PID|||141" +
            "2199415782^80756000080069520760~CSS^80756000080069520760~CSS^KSGR&KSGR-DISPO-PROD&2.16.756.5.30.1.456.1&ISO^PI^KSGR~7569999888877^^^AHV Nummer neu&2.16.756.5.32&ISO^SS||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\n" +
            "PV1||I^^|Sta3^200.1^^KSGR||||4711^Langer^Bernhard^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4713^Langhardt^Peter^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6||||||||||1234567890|Allgemein|KVG|||||||||||||||||||||||201805241030|\n";

    MessageStore messageStore;

    @Before
    public void init() {
        messageStore = new MessageStoreImpl();
        messageStore.clear();
    }

    @Test
    public void testAddMessage() throws Exception {
        messageStore.addReceivedMessage(msg1);
        messageStore.addReceivedMessage(msg2);

        assertEquals(messageStore.getReceivedMessage("12EF35AB489aa").getMessageAsString(), msg1);
        assertEquals(messageStore.getReceivedMessage("12EF35AB489aabb").getMessageAsString(), msg2);
        assertNull(messageStore.getReceivedMessage("kk12EF35AB489aabb"));

        assertEquals(messageStore.getMessagesReceived().size(), 2);
    }

    @Test
    public void testRemoveMessage() throws Exception {
        messageStore.addReceivedMessage(msg1);
        messageStore.addReceivedMessage(msg2);

        messageStore.removeReceivedMessage("12EF35AB489aa");

        assertNull(messageStore.getReceivedMessage("12EF35AB489aa"));
        assertEquals(messageStore.getReceivedMessage("12EF35AB489aabb").getMessageAsString(), msg2);
        assertEquals(messageStore.getMessagesReceived().size(), 1);
    }

}
