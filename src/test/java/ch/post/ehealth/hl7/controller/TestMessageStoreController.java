package ch.post.ehealth.hl7.controller;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import ch.post.ehealth.hl7.Application;
import ch.post.ehealth.hl7.messagestore.MessageStore;
import ch.post.ehealth.hl7.model.ReceivedMessage;
import ch.post.ehealth.hl7.util.MessageUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class TestMessageStoreController {
    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    final String msg = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489aa||2.2|||||||||\n" +
            "EVN||20180522102452|||KSGR^Muster^Fritz^012344||\n" +
            "PID|||1412199415782^^^SIVC-PAS-PROD^PI^SIVC~7569999888877^^^NeueAHVNr^SS^NeueAHVNr~80756000080069520760^^^VK_NR^GV^VK_NR~80756000069520760^^^VK_NR^ZV^VK_NR||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\n" +
            "ROL||AD|PP|123456^^^^^^^^Healthcase Provider Directory||||||||||\n" +
            "PV1||I^stationaer|Sta3^200.1^^KSGR|||||4712^Lang^Franz^^^Dr.|4712^Lang^Franz^^^Dr.||||||||||1234567890^^^KSGR|102|KVG|||||||||||||||||||||||201805241030|\n\r";

    final String msg2 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489bb||2.2|||||||||\n" +
            "EVN||20190522102452|||KSGR^Muster^Fritz^012344||\n" +
            "PID|||1412199415782^^^SIVC-PAS-PROD^PI^SIVC~7569999888877^^^NeueAHVNr^SS^NeueAHVNr~80756000080069520760^^^VK_NR^GV^VK_NR~80756000069520760^^^VK_NR^ZV^VK_NR||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\n" +
            "PV1||I^stationaer|Sta3^200.1^^KSGR|||||4712^Lang^Franz^^^Dr.^^^^^^^^^^^^^|4712^Lang^Franz^^^Dr.^^^^^^^^^^^^^||||||||||1234567890^^^KSGR|102|KVG|||||||||||||||||||||||201805241030|\n\n";

    final String msg3 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489cc||2.2|||||||||\n" +
            "EVN||20190522102452|||KSGR^Musterrrrr^Fritzzzzzz^012344||\n" +
            "PID|||1412199415782^^^SIVC-PAS-PROD^PI^SIVC~7569999888877^^^NeueAHVNr^SS^NeueAHVNr~80756000080069520760^^^VK_NR^GV^VK_NR~80756000069520760^^^VK_NR^ZV^VK_NR||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\n" +
            "PV1||I^stationaer|Sta3^200.1^^KSGR|||||4712^Lang^Franz^^^Dr.^^^^^^^^^^^^^|4712^Lang^Franz^^^Dr.^^^^^^^^^^^^^||||||||||1234567890^^^KSGR|102|KVG|||||||||||||||||||||||201805241030|\n\n";

    private MockMvc mockMvc;
    @Autowired
    MessageStore messageStore;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.stream(converters)
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        messageStore.clear();
        messageStore.addReceivedMessage(msg);
        messageStore.addReceivedMessage(msg2);
    }

    @Test
    public void testGetReceivedMessage() throws Exception {
        String msgId = MessageUtil.getInstance().getMessageIdFromMessage(msg);
        mockMvc.perform(get("/messages/message/" + msgId))
                .andDo(print(System.out))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.messageAsString", is(msg)))
                .andExpect(jsonPath("$.messageId", is(msgId)));

        msgId = MessageUtil.getInstance().getMessageIdFromMessage(msg2);
        mockMvc.perform(get("/messages/message/" + msgId))
                .andDo(print(System.out))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.messageAsString", is(msg2)))
                .andExpect(jsonPath("$.messageId", is(msgId)));

    }

    @Test
    public void testGetAllReceivedMessages() throws Exception {
        String msgId1 = MessageUtil.getInstance().getMessageIdFromMessage(msg);
        String msgId2 = MessageUtil.getInstance().getMessageIdFromMessage(msg2);

        int expectedSize = messageStore.getMessagesReceived().size();

        mockMvc.perform(get("/messages/all"))
                .andDo(print(System.out))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(expectedSize)))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].messageAsString", is(msg)))
                .andExpect(jsonPath("$[0].messageId", is(msgId1)))
                .andExpect(jsonPath("$[1].messageAsString", is(msg2)))
                .andExpect(jsonPath("$[1].messageId", is(msgId2)));
    }

    @Test
    public void testAddMessage() throws Exception {
        String msg3Id = MessageUtil.getInstance().getMessageIdFromMessage(msg3);
        ReceivedMessage testMessage = new ReceivedMessage(msg3);
        String testMessageJson = json(testMessage);

        this.mockMvc.perform(post("/messages/message")
                .contentType(contentType)
                .content(testMessageJson))
                .andDo(print(System.out))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.messageId", is(msg3Id)));

        assertEquals(messageStore.getReceivedMessage(msg3Id).getMessageAsString(), msg3);

        mockMvc.perform(get("/messages/all"))
                .andDo(print(System.out))
                .andExpect(content().contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[2].messageAsString", is(msg3)))
                .andExpect(jsonPath("$[2].messageId", is(msg3Id)));

    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
