package ch.post.ehealth.hl7.util;

import ca.uhn.hl7v2.model.Message;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MessageUtilTester {
    final String msg1 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489aa||2.2|||||||||\n" +
            "EVN||20180522102452|||KSGR^Muster^Fritz^012344||\n" +
            "PID|||1412199415782^80756000080069520760~CSS^80756000080069520760~CSS^KSGR&KSGR-DISPO-PROD&2.16.756.5.30.1.456.1&ISO^PI^KSGR~7569999888877^^^AHV Nummer neu&2.16.756.5.32&ISO^SS||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\n" +
            "PV1||I^^|Sta3^200.1^^KSGR||||4711^Langer^Bernhard^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4713^Langhardt^Peter^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6||||||||||1234567890|Allgemein|KVG|||||||||||||||||||||||201805241030|\n";

    final String msg2 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489aabb||2.2|||||||||\n" +
            "EVN||20180522102452|||KSGR^Muster^Fritz^012344567||\n" +
            "PID|||1412199415782^80756000080069520760~CSS^80756000080069520760~CSS^KSGR&KSGR-DISPO-PROD&2.16.756.5.30.1.456.1&ISO^PI^KSGR~7569999888877^^^AHV Nummer neu&2.16.756.5.32&ISO^SS||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\n" +
            "PV1||I^^|Sta3^200.1^^KSGR||||4711^Langer^Bernhard^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4713^Langhardt^Peter^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6||||||||||1234567890|Allgemein|KVG|||||||||||||||||||||||201805241030|\n";

    final String msg3 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489aabb||2.2|||||||||\r" +
            "EVN||20180522102452|||KSGR^Muster^Fritz^012344567||\r" +
            "PID|||1412199415782^80756000080069520760~CSS^80756000080069520760~CSS^KSGR&KSGR-DISPO-PROD&2.16.756.5.30.1.456.1&ISO^PI^KSGR~7569999888877^^^AHV Nummer neu&2.16.756.5.32&ISO^SS||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com^||||||||||||||||\r" +
            "PV1||I^^|Sta3^200.1^^KSGR||||4711^Langer^Bernhard^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4713^Langhardt^Peter^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6||||||||||1234567890|Allgemein|KVG|||||||||||||||||||||||201805241030|\r";

    final String strippedMsg3 = "MSH|^~\\&|SIVC-PAS-PROD^|SIVC^|PIX/PDQ Manager^||20180522102452||ADT^A01|12EF35AB489aabb||2.2" +
            "EVN||20180522102452|||KSGR^Muster^Fritz^012344567" +
            "PID|||1412199415782^80756000080069520760~CSS^80756000080069520760~CSS^KSGR&KSGR-DISPO-PROD&2.16.756.5.30.1.456.1&ISO^PI^KSGR~7569999888877^^^AHV Nummer neu&2.16.756.5.32&ISO^SS||Meier^Franz^Hanspeter^^Junior||20080727|M|||Bahnhofstrasse 17^^Chur^^7000^Schweiz||+41815300689^+41792338481^p.braunschweiler@gmail.com^+41792338481|+41815300689^+41792338481^p.braunschweiler@gmail.com" +
            "PV1||I^^|Sta3^200.1^^KSGR||||4711^Langer^Bernhard^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4712^Lang^Franz^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6|4713^Langhardt^Peter^^^Dr.^^^2.16.756.5.30.1.166.12.1.3.1.6^L^^^^^^2.16.756.5.30.1.166.12.1.3.1.6||||||||||1234567890|Allgemein|KVG|||||||||||||||||||||||201805241030";

    @Test
    public void testGetMessageIdFromMessage() throws Exception {
        String messageId = MessageUtil.getInstance().getMessageIdFromMessage(msg1);
        assertEquals(messageId, "12EF35AB489aa");

        messageId = MessageUtil.getInstance().getMessageIdFromMessage(msg2);
        assertEquals(messageId, "12EF35AB489aabb");

        Message message = MessageUtil.getInstance().getMessageFromString(msg1);
        messageId = MessageUtil.getInstance().getMessageIdFromMessage(message);
        assertEquals(messageId, "12EF35AB489aa");
    }

    @Test
    public void testStripMessage() throws Exception {
        String strippedMsg = MessageUtil.getInstance().stripMessage(msg3);
        assertEquals(strippedMsg3, strippedMsg);
    }

}
